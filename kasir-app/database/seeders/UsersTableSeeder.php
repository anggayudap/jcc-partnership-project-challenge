<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder {
    public function run() {
        User::create([
            'name'     => 'Admin Kasir',
            'username' => 'admin',
            'email'    => 'admin@bogakasir.id',
            'password' => Hash::make('admin'),
            'role_id'  => 1
        ]);
        User::create([
            'name'     => 'Kasir',
            'username' => 'kasir',
            'email'    => 'kasir@bogakasir.id',
            'password' => Hash::make('kasir'),
            'role_id'  => 2
        ]);
    }
}
