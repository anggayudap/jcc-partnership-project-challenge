@extends('layouts.template')
@section('title','Mulai Transaksi Pembelian')

@push('style')
<!-- SweetAlert & Select2-->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/css/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css')}}">
    <style>
        input[type=number]::-webkit-outer-spin-button,
        input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type=number] {
            -moz-appearance:textfield;
        }
    </style>
@endpush

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Transaksi Pembelian</h4>
            </div>
            <div class="card-body">
                @livewire('transaksi-pembelian')
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-success btn-icon-split" id="btn-submit" form="form-create" onclick="submit_transaksi()" disabled>
                        <span class="icon text-white-50">
                            <i class="fas fa-cart-arrow-down"></i>
                        </span>
                        <span class="text">Akhiri Transaksi</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('script')
<!-- Script Tambahan -->
    <script src="{{ asset('plugins/sweetalert2/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/id.js') }}"></script>
    <script type="text/javascript">
        $('[data-toggle="tooltip"]').tooltip();
        $(document).on('select2:open', () => {
            document.querySelector('.select2-search__field').focus();
        });

        setInterval(() => {
            if ($('input[name=total]').val() > 0) {
                $('#btn-submit').prop('disabled', false);
            } else {
                $('#btn-submit').prop('disabled', true);
            }
        }, 1000);

        function submit_transaksi() {
            event.preventDefault();
            Swal.fire({
                width: 680,
                title: 'Selesai berbelanja?',
                text: "Memilih 'iya' akan menyelesaikan transaksi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Iya',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value == true) {
                    Swal.fire({
                        width: 680,
                        title: 'Ingin mencetak invoice?',
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Iya',
                        cancelButtonText: 'Tidak'
                    }).then((result) => {
                        if (result.value == true) {
                            $('input[name=print]').attr('disabled', false);
                            $(`#form-create`).submit();
                        } else {
                            $(`#form-create`).submit();
                        }
                    });
                } else {
                    return false;
                }
            });
        }
    </script>
@endpush
