@extends('layouts.template')
@section('title', 'Detail Transaksi')
@push('style')
<!-- SweetAlert -->
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/css/sweetalert2.min.css')}}">
    <style>
        .btn-pink{
            color: #fff;
            background-color: #e83e8c;
            border-color: #e83e8c;
        }
        .btn-pink:hover {
            color: #fff;
        }
    </style>
@endpush

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-7">
                        <h4 class="card-title">Detail Transaksi</h4>
                    </div>
                    <div class="col-lg-5">
                        <div class="text-right">
                            <a href="{{ route('pembelian.history') }}" class="btn btn-secondary btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-arrow-left"></i>
                                </span>
                                <span class="text">Kembali</span>
                            </a>
                            <a href="{{ route('pembelian.print', [$data_pembelian->id]) }}" class="btn btn-pink btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-print"></i>
                                </span>
                                <span class="text">Print</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <div>Dilayani Oleh : <strong>{{ $data_pembelian->user->name }}</strong></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="text-right">Tanggal Transaksi : <strong>{{ tanggalTransaksi($data_pembelian->created_at) }}</strong></div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr role="row" style="background-color:#dadada">
                                <th class="text-center" style="width: 5%;">No</th>
                                <th>Nama Barang</th>
                                <th class="text-center" style="width: 10%;">Jumlah</th>
                                <th class="text-right">Harga Satuan</th>
                                <th class="text-right">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_barang as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->barang->nama_barang }}</td>
                                <td class="text-center">{{ $item->jumlah }}</td>
                                <td class="text-right">{{ formatRupiah($item->barang->harga_satuan, 0) }}</td>
                                <td class="text-right">{{ formatRupiah($item->subtotal, 0) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot role="row" style="background-color:#dadada">
                            <th colspan="4" class="text-right" style="font-size: 22px;"> <span class="text-black"> Total </span></th>
                            <th class="text-right"  style="font-size: 22px;">{{ formatRupiah($data_pembelian->total_harga, 0) }}</th>
                        </tfoot>
                    </table>
                </div>
                <div>
                    <div class="text-primary"> Unduh invoice dengan menekan tombol <strong> Print </strong> lalu pilih opsi <strong> "Simpan sebagai PDF". </strong></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
<!-- Script Tambahan -->
    <script src="{{ asset('plugins/sweetalert2/js/sweetalert2.all.min.js') }}"></script>
    <script type="text/javascript">
        function submit_delete(user_id) {
            event.preventDefault();
            Swal.fire({
                width: 680,
                title: 'Anda yakin ingin menghapus data terpilih?',
                text: "Data yang sudah di hapus tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value == true) {
                    $(`#form-delete-${user_id}`).submit();
                } else {
                    return false;
                }
            });
        }
    </script>
@endpush