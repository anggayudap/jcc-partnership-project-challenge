@extends('layouts.template')
@section('title', 'Riwayat Transaksi')
@push('style')
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/css/sweetalert2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <style>
        input[type=number]::-webkit-outer-spin-button,
        input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type=number] {
            -moz-appearance:textfield;
        }

        .btn-pink{
            color: #fff;
            background-color: #e83e8c;
            border-color: #e83e8c;
        }
        .btn-pink:hover {
            color: #fff;
        }

        .form-check-label {
            margin-bottom: 0;
            margin-right: 0.5em;
        }
    </style>
@endpush

@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-7">
                        <h4 class="card-title">Riwayat Transaksi</h4>
                    </div>
                    <div class="col-lg-5">
                        <div class="text-right">
                            <h6>Hari ini : {{ tanggalTransaksi(now(), true) }}</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <span data-toggle="modal" data-target="#modalFilter">
                            <a class="btn btn-info btn-icon-split mr-1" 
                                data-toggle="tooltip" data-placement="top" title="Filter Transaksi">
                                <span class="icon text-white-50">
                                    <i class="fas fa-filter"></i> 
                                </span>
                                <span class="text">Filter</span>
                            </a>
                        </span>
                        @if (Auth::user()->role_id == 1)
                            <a href="{{ url('/pembelian/riwayat?mulai=' . date('Y-m-d') . '&akhir=' . date('Y-m-d')) }}" class="btn btn-primary btn-icon-split mr-1"
                                data-toggle="tooltip" data-placement="top" title="Lihat Transaksi Hari Ini">
                                <span class="icon text-white-50">
                                    <i class="far fa-calendar-check"></i> 
                                </span>
                                <span class="text">Transaksi Hari Ini</span>
                            </a>
                        @endif
                        @if (request('harga') || request('mulai') || request('barang') || request('sort'))
                            <a href="{{ route('pembelian.history') }}" class="btn btn-danger btn-icon-split mr-1"
                                data-toggle="tooltip" data-placement="top" title="Bersihkan Filter">
                                <span class="icon text-white-50">
                                    <i class="fas fa-broom"></i> 
                                </span>
                                <span class="text">Bersihkan Filter</span>
                            </a>
                        @endif
                    </div>
                </div>
                @if (request('harga') || request('mulai') || request('barang') || request('sort'))
                    <p class="text-secondary">
                        Filter saat ini : <br>
                        @if (request('harga'))
                            &emsp;&emsp; Total harga : <strong> {{ (request('op') == 'lt' ? 'Kurang dari ' : (request('op') == 'gt' ? 'Lebih dari ' : 'Sama dengan ')) . formatRupiah(request('harga'), 0) }} </strong> <br>
                        @endif
                        @if (request('mulai'))
                            @if (request('mulai') == request('akhir'))
                                &emsp;&emsp; Pada tanggal : <strong> {{ tanggalTransaksi(request('mulai'), true) }} </strong> <br>
                            @else
                                &emsp;&emsp; Dari tanggal : <strong> {{ tanggalTransaksi(request('mulai'), true) }} hingga {{ tanggalTransaksi(request('akhir'), true) }} </strong> <br>
                            @endif
                        @endif
                        @if (request('user'))
                            &emsp;&emsp; Dilayani oleh salah satu kasir diantara : <strong>
                            @for ($i = 0; $i < count($filtered_user); $i++)
                                @if ($i == count($filtered_user) - 1)
                                    {{ $filtered_user[$i]}}
                                @else
                                    {{ $filtered_user[$i] . ', '}}
                                @endif
                            @endfor </strong> <br>
                        @endif
                        @if (request('barang'))
                            &emsp;&emsp; Membeli salah satu barang diantara : <strong>
                            @for ($i = 0; $i < count($filtered_barang); $i++)
                                @if ($i == count($filtered_barang) - 1)
                                    {{ $filtered_barang[$i]}}
                                @else
                                    {{ $filtered_barang[$i] . ', '}}
                                @endif
                            @endfor </strong> <br>
                        @endif
                        @if (request('sort') && request('sort_by') && request('sort_type'))
                            &emsp;&emsp; Urutkan berdasarkan : 
                            @if (request('sort_by') == 'waktu')
                                Waktu transaksi dari <strong> {{ request('sort_type') == 'asc' ? 'terlama ke terbaru' : 'terbaru ke terlama'}} </strong> <br>
                            @else
                                Total harga dari <strong> {{ request('sort_type') == 'asc' ? 'terkecil ke terbesar' : 'terkecil ke terbesar'}} </strong> <br>
                            @endif
                        @endif
                    </p>
                @endif
                @if ($data->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr role="row" style="background-color:#dadada">
                                    <th class="text-center" style="width: 5%;">No</th>
                                    <th>ID Transaksi</th>
                                    <th>Dilayani Oleh</th>
                                    <th>Waktu Transaksi</th>
                                    <th class="text-right">Total Harga</th>
                                    <th class="text-center" style="width: 11%;"><i class="fas fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                <tr>
                                    <td>{{ (($data->currentPage() - 1) * $data->perPage()) + $loop->iteration }}</td>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->user->name }}</td>
                                    <td>{{ tanggalTransaksi($item->created_at) }}</td>
                                    <td class="text-right">{{ formatRupiah($item->total_harga, 0) }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('pembelian.detail', [$item->id]) }}"
                                            class="btn btn-sm btn-info btn-action mr-1" data-container="table"
                                            data-toggle="tooltip" data-placement="top" title="Detail Transaksi">
                                            <i class="fas fa-info-circle"></i>
                                        </a>
                                        <a href="{{ route('pembelian.print', [$item->id]) }}"
                                            class="btn btn-sm btn-pink btn-action ml-1" data-container="table"
                                            data-toggle="tooltip" data-placement="top" title="Print Invoice">
                                            <i class="fas fa-print"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if (Auth::user()->role_id != 1)
                        <h6 class="text-secondary">Transaksi yang dilayani oleh kasir hanya dapat dilihat oleh kasir itu sendiri dan hanya pada hari saat transaksi dilakukan.</h6>
                    @endif
                    <div class="d-flex justify-content-center mt-3">
                        {!! $data->links() !!}
                    </div>
                @else
                    <div class="text-center">
                        <h3>Tidak ada data ditemukan.</h3>
                        <img src="{{ asset('img/data_empty.jpg') }}" width="427px" height="240px">
                        @if (Auth::user()->role_id != 1)
                            <h6 class="text-secondary">Transaksi yang dilayani oleh kasir hanya dapat dilihat oleh kasir itu sendiri dan hanya pada hari saat transaksi dilakukan.</h6>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalFilter" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Filter Data</h5>
                    <button class="close" type="button" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('pembelian.history') }}" class="form-horizontal" id="form-filter">
                        <div class="text-secondary small mb-2">Filter dapat diisi semua atau hanya salah satunya.</div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Perbandingan Harga :</label>
                            <div class="col-lg-8">
                                <select class="form-control" name="op">
                                    <option value="" disabled selected>--Pilih salah satu--</option>
                                    <option value="lt">Kurang dari</option>
                                    <option value="gt">Lebih dari</option>
                                    <option value="eq">Sama dengan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Total Harga :</label>
                            <div class="col-lg-8">
                                <input type="number" class="form-control" name="harga" placeholder="Total Harga" autocomplete="off">
                            </div>
                        </div>
                        @if (Auth::user()->role_id == 1)
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Tanggal Transaksi :</label>
                                <div class="col-lg-8">
                                    <div id="datepicker-cari-tanggal">
                                        <div class="input-group input-daterange">
                                            <input type="text" class="form-control" name="mulai" placeholder="Tanggal Mulai" autocomplete="off">
                                            <span class="input-group-text">sampai</span>
                                            <input type="text" class="form-control" name="akhir" placeholder="Tanggal Akhir" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Dilayani oleh :</label>
                            <div class="col-lg-8">
                                <select name="user[]" multiple="multiple" id="select2-user">
                                    @foreach ($user as $item)
                                    <option value="{{ $item->id }}"> {{ $item->name }}
                                        ({{ $item->role->name }})</option>
                                    @endforeach
                                </select>
                            </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Barang :</label>
                            <div class="col-lg-8">
                                <select name="barang[]" multiple="multiple" id="select2-barang">
                                    @foreach ($barang as $item)
                                    <option value="{{ $item->id }}"> {{ $item->nama_barang }}
                                        ({{ formatRupiah($item->harga_satuan, 0) }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-4 col-form-label">Urutkan :</label>
                            <div class="col-lg-8">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="sort-true" name="sort" value="true">
                                    <label class="form-check-label" for="sort-true">Ya</label>
                                    <input class="form-check-input" type="radio" id="sort-false" name="sort" value="false" checked>
                                    <label class="form-check-label" for="sort-false">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <div class="sort-option" style="display: none;">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Urut Berdasarkan :</label>
                                <div class="col-lg-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="sort-by-waktu" name="sort_by" value="waktu">
                                        <label class="form-check-label" for="sort-by-waktu">Waktu Transaksi</label>
                                        <input class="form-check-input" type="radio" id="sort-by-harga" name="sort_by" value="harga">
                                        <label class="form-check-label" for="sort-by-harga">Total Harga</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Jenis Urutan :</label>
                                <div class="col-lg-8">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" id="sort-type-desc" name="sort_type" value="desc">
                                        <label class="form-check-label" id="label-sort-1" for="sort-type-desc">Terbesar ke terkecil</label>
                                        <input class="form-check-input" type="radio" id="sort-type-asc" name="sort_type" value="asc">
                                        <label class="form-check-label" id="label-sort-2" for="sort-type-asc">Terkecil ke terbesar</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <input class="btn btn-success" type="submit" value="Filter" form="form-filter">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('plugins/sweetalert2/js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/js/id.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.id.min.js') }}"></script>
    <script type="text/javascript">
        $('[data-toggle="tooltip"]').tooltip();

        $('input[name=harga]').on('keyup', function () {
            $('input[name=harga]').val() != '' ? $('select[name=op]').prop('required', true) : $('select[name=op]').prop('required', false)
        });

        $('select[name=op]').on('change', function () {
            $('input[name=harga]').prop('required', true);
        });

        $('input[name=sort]').on('change', function () {
            if($('input[name=sort]:checked').val() == 'true') {
                $('.sort-option').show();
                $('input[name=sort_by]').prop('required', true);
                $('input[name=sort_by]').prop('disabled', false);
                $('input[name=sort_type]').prop('required', true);
                $('input[name=sort_type]').prop('disabled', false);
            } else {
                $('.sort-option').hide();
                $('input[name=sort_by]').prop('required', false);
                $('input[name=sort_by]').prop('disabled', true);
                $('input[name=sort_type]').prop('required', false);
                $('input[name=sort_type]').prop('disabled', true);
            }
        });

        $('input[name=sort_by]').on('change', function () {
            if($('input[name=sort_by]:checked').val() == 'waktu') {
                $('#label-sort-1').html('Terbaru ke terlama');
                $('#label-sort-2').html('Terlama ke terbaru');
            } else {
                $('#label-sort-1').html('Terbesar ke terkecil');
                $('#label-sort-2').html('Terkecil ke terbesar');
            }
        });

        $('#form-filter').on('submit', function () {
            $('input[name=harga]').val() == '' ? $('input[name=harga]').removeAttr('name') : false;
            $('input[name=mulai]').val() == '' ? $('input[name=mulai]').removeAttr('name') : false;
            $('input[name=akhir]').val() == '' ? $('input[name=akhir]').removeAttr('name') : false;
            $('#select2-barang').select2('val').length == 0 ? $('#select2-barang').removeAttr('name') : false;
        });

        $(document).ready(function () {
            $('#datepicker-cari-tanggal .input-daterange').datepicker({
                format: 'yyyy/mm/dd',
                todayBtn: "linked",
                autoclose: true,
                todayHighlight: true,
                endDate : '0d'
            });
            $('#select2-barang').select2({
                language: 'id',
                width: '100%'
            });
            $('#select2-user').select2({
                language: 'id',
                width: '100%'
            });
        });

        function submit_delete(user_id) {
            event.preventDefault();
            Swal.fire({
                width: 680,
                title: 'Anda yakin ingin menghapus data terpilih?',
                text: "Data yang sudah di hapus tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value == true) {
                    $(`#form-delete-${user_id}`).submit();
                } else {
                    return false;
                }
            });
        }
    </script>
@endpush