@extends('layouts.template')
@section('title', 'Dashboard')
@section('page-title', 'Dashboard')
@push('style')
    <style>
        .border-left-purple{
            border-top: .25rem solid #9a31ff !important
        }
        .border-top-primary{
            border-top: .25rem solid #4e73df !important
        }
        .border-top-danger{
            border-top: .25rem solid #e74a3b !important
        }
    </style>
@endpush
@section('content')
    @if (Auth::user()->role_id == 1)
        <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
                <a href="{{ route('pembelian.start') }}" class="text-white">
                    <div class="card border-left-purple shadow h-100 py-2" style="background-color: #9a31ff;">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="h4 font-weight-bold"> Mulai Transaksi </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning mb-1">
                                    Jumlah Transaksi Hari Ini</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $transactionToday }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-file-invoice-dollar fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-danger shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-danger mb-1">
                                    Jumlah Pendapatan Hari Ini</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ formatRupiah($incomeToday, 0) }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="far fa-money-bill-alt fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success mb-1">
                                    Jumlah Pendapatan Minggu Ini</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ formatRupiah($incomeThisWeek, 0) }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-wallet fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
                <a href="{{ route('pembelian.start') }}" class="text-white">
                    <div class="card border-left-purple shadow h-100 py-2" style="background-color: #9a31ff;">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="h4 font-weight-bold"> Mulai Transaksi </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning mb-1">
                                    Transaksi Terlayani Hari Ini</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $transactionToday }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-file-invoice-dollar fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if (Auth::user()->role_id == 1)
        <div class="row">
            <div class="col-xl-6 col-md-12 mb-4">
                <div class="card border-top-primary shadow h-100">
                    <div class="card-header">
                        <h4 class="card-title">5 Produk Terlaris Minggu Ini</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr role="row">
                                        <th class="text-center">Rank</th>
                                        <th>Produk</th>
                                        <th class="text-center">Terjual</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($top5ProductThisWeek as $item)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td>{{ $item->nama_barang }}</td>
                                            <td class="text-center">{{ $item->terjual }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 mb-4">
                <div class="card border-top-danger shadow h-100">
                    <div class="card-header">
                        <h4 class="card-title">5 Transaksi Terbesar Minggu Ini</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr role="row">
                                        <th class="text-center">Rank</th>
                                        <th>Tanggal Transaksi</th>
                                        <th class="text-right">Total Harga</th>
                                        <th class="text-center">Detail</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($top5TransactionThisWeek as $item)
                                        <tr>
                                            <td class="text-center">{{ $loop->iteration }}</td>
                                            <td>{{ tanggalTransaksi($item->tanggal, true) }}</td>
                                            <td class="text-right">{{ formatRupiah($item->total_harga, 0) }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('pembelian.detail', [$item->id]) }}"
                                                    class="btn btn-sm btn-info btn-action mr-1" data-container="table"
                                                    data-toggle="tooltip" data-placement="top" title="Detail Transaksi">
                                                    <i class="fas fa-info-circle"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection