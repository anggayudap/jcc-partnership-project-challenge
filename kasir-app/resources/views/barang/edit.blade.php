@extends('layouts.template')
@section('title', 'Edit Data Barang')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-7">
                        <h4 class="card-title">Edit Data Barang</h4>
                    </div>
                    <div class="col-lg-5">
                        <div class="text-right">
                            <a href="{{ route('barang.index') }}" class="btn btn-secondary btn-icon-split">
                                <span class="icon text-white-50">
                                    <i class="fas fa-arrow-left"></i>
                                </span>
                                <span class="text">Kembali</span>
                            </a>
                            <button type="button" class="btn btn-warning btn-icon-split" id="btn-edit">
                                <span class="icon text-white-50">
                                    <i class="fas fa-edit"></i>
                                </span>
                                <span class="text">Edit</span>
                            </button>
                            <button type="button" class="btn btn-danger btn-icon-split" id="btn-reset" style="display: none">
                                <span class="icon text-white-50">
                                    <i class="fas fa-times"></i>
                                </span>
                                <span class="text">Reset</span>
                            </button>
                            <button type="submit" class="btn btn-success btn-icon-split" id="btn-submit" form="form-update" disabled>
                                <span class="icon text-white-50">
                                    <i class="fas fa-save"></i>
                                </span>
                                <span class="text">Simpan</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('barang.update', [$data->id]) }}" method="post" class="form-horizontal" id="form-update">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Nama Barang :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="nama_barang" maxlength="50" placeholder="Nama Barang" autocomplete="off" required value="{{ $data->nama_barang }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Harga Satuan :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="harga_satuan" id="harga_satuan" placeholder="Harga Satuan" autocomplete="off" required value="{{ formatRupiah($data->harga_satuan, 0) }}" onkeyup="formatRupiah(this.id)" readonly>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="card mt-2">
                <div class="card-body">
                    <h5>Terdapat kesalahan: </h5>
                    <div class="text-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function (){
            $('#btn-edit').on('click', function () {
                $("input[name=nama_barang]").attr('readonly', false);
                $("input[name=harga_satuan]").attr('readonly', false);
                $('#btn-edit').hide();
                $('#btn-reset').show();
                $('#btn-submit').attr('disabled', false);
            });
            $('#btn-reset').on('click', function () {
                $("input[name=nama_barang]").attr('readonly', true);
                $("input[name=harga_satuan]").attr('readonly', true);
                $("input[name=nama_barang]").val('{{ $data->nama_barang }}');
                $("input[name=harga_satuan]").val('{{ formatRupiah($data->harga_satuan, 0) }}');
                $('#btn-reset').hide();
                $('#btn-edit').show();
                $('#btn-submit').attr('disabled', true);
            });

            $('#form-update').on('submit', function () {
                let raw = $('input[name=harga_satuan]').val();
                let formatted = raw.replace(/rp|[.]/gi, "");
                $('input[name=harga_satuan]').val(formatted);
            });
        });

        function formatRupiah(id) {
            let angka = document.getElementById(id).value;
            let number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            document.getElementById(id).value = 'Rp' + rupiah;
        }
    </script>
@endpush