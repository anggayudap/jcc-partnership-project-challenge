@extends('layouts.template')
@section('title', 'Profil')
@section('content')
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-7">
                        <h4 class="card-title">Profil</h4>
                    </div>
                    <div class="col-lg-5">
                        <div class="text-right">
                            <button type="button" class="btn btn-warning btn-icon-split" id="btn-edit">
                                <span class="icon text-white-50">
                                    <i class="fas fa-edit"></i>
                                </span>
                                <span class="text">Edit</span>
                            </button>
                            <button type="button" class="btn btn-danger btn-icon-split" id="btn-reset" style="display: none">
                                <span class="icon text-white-50">
                                    <i class="fas fa-times"></i>
                                </span>
                                <span class="text">Reset</span>
                            </button>
                            <button type="submit" class="btn btn-success btn-icon-split" id="btn-submit" form="form-update" disabled>
                                <span class="icon text-white-50">
                                    <i class="fas fa-save"></i>
                                </span>
                                <span class="text">Simpan</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('user.profile_update')}}" method="post" class="form-horizontal" id="form-update">
                    @csrf
                    @method('PUT')
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Nama Kasir :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="name" maxlength="100" placeholder="Nama Kasir" autocomplete="off" required value="{{ $data->name }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Username :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="username" maxlength="50" placeholder="Username" autocomplete="off" required value="{{ $data->username }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Email :</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="email" maxlength="50" placeholder="Email" autocomplete="off" required value="{{ $data->email }}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label">Password :</label>
                        <div class="col-lg-4">
                            <input type="password" class="form-control" name="password" maxlength="50" placeholder="Password" autocomplete="off" readonly>
                            <small class="text-secondary">Kosongkan password jika tidak ingin mengubahnya.</small>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="card mt-2">
                <div class="card-body">
                    <h5>Terdapat kesalahan: </h5>
                    <div class="text-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function (){
            $('#btn-edit').on('click', function () {
                $("input[name=name]").attr('readonly', false);
                $("input[name=username]").attr('readonly', false);
                $("input[name=email]").attr('readonly', false);
                $("input[name=password]").attr('readonly', false);
                $('#btn-edit').hide();
                $('#btn-reset').show();
                $('#btn-submit').attr('disabled', false);
            });
            $('#btn-reset').on('click', function () {
                $("input[name=name]").attr('readonly', true);
                $("input[name=username]").attr('readonly', true);
                $("input[name=email]").attr('readonly', true);
                $("input[name=password]").attr('readonly', true);
                $("input[name=name]").val('{{ $data->name }}');
                $("input[name=username]").val('{{ $data->username }}');
                $("input[name=email]").val('{{ $data->email }}');
                $('#btn-reset').hide();
                $('#btn-edit').show();
                $('#btn-submit').attr('disabled', true);
            });
        });
    </script>
@endpush