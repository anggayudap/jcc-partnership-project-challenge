<?php

use Illuminate\Support\Carbon;

function formatRupiah($number, $decimals = 2){ 
    return 'Rp' . number_format($number, $decimals, ',' , '.');
}

function tanggalTransaksi($timestamp, $only_date = false)
{
    return Carbon::parse($timestamp)->locale('id')->isoFormat($only_date ? 'D MMMM YYYY' : 'dddd, D MMMM YYYY HH:mm');
}