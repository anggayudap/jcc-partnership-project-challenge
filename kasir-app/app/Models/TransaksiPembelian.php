<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransaksiPembelian extends Model
{
    use HasFactory;
    protected $table = 'transaksi_pembelian';
    protected $guarded = ['id'];

    public function pembelian_barang()
    {
        return $this->hasMany(TransaksiPembelianBarang::class, 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeSearch($query)
    {
        if (request('harga'))
        {
            $query->where('total_harga', (request('op') == 'lt' ? '<' : (request('op') == 'gt' ? '>' : '=')), request('harga'));
        }
        if (request('mulai') && request('akhir'))
        {
            if (request('mulai') == request('akhir'))
            {
                $query->whereDate('transaksi_pembelian.created_at', request('mulai'));
            } else {
                $query->whereBetween('transaksi_pembelian.created_at', [request('mulai'), request('akhir')]);
            }
        }
        if (request('user'))
        {
            $query->whereIn('user_id', request('user'));
        }
        if (request('barang'))
        {
            $query->join('transaksi_pembelian_barang','transaksi_pembelian.id', '=', 'transaksi_pembelian_barang.transaksi_pembelian_id')
            ->select('transaksi_pembelian.id', 'total_harga', 'user_id', 'master_barang_id', 'transaksi_pembelian.created_at', 'transaksi_pembelian.updated_at')
            ->whereIn('master_barang_id', request('barang'))
            ->groupBy('transaksi_pembelian_barang.transaksi_pembelian_id');
        }
        if(request('sort') && request('sort_by') && request('sort_type'))
        {
            $query->orderBy((request('sort_by') == 'waktu' ? 'transaksi_pembelian.created_at' : 'transaksi_pembelian.total_harga'), request('sort_type'));
        }
        return $query;
    }
    
    public function scopeRole($query)
    {
        if (Auth::user()->role_id != 1) {
            return $query->where('user_id', Auth::user()->id)
                    ->whereDate('transaksi_pembelian.created_at', date('Y-m-d'));
        }
    }
}