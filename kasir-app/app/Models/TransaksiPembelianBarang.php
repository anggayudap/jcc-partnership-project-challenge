<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPembelianBarang extends Model
{
    use HasFactory;
    protected $table = 'transaksi_pembelian_barang';
    protected $guarded = ['id'];

    public function pembelian()
    {
        return $this->belongsTo(TransaksiPembelian::class, 'transaksi_pembelian_id');
    }

    public function barang()
    {
        return $this->belongsTo(MasterBarang::class, 'master_barang_id');
    }
}
