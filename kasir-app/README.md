# Aplikasi Kasir (Boga Kasir)

## Feature

* Calculation Transaction in live :moneybag:
* Filter with many option :white_check_mark:
* Print dan Download PDF Invoice Transaction :page_facing_up:
* User friendly interface :computer:

## Library

* [Laravel Sweetalert](https://github.com/realrashid/sweet-alert)
* [Laravel Livewire](https://github.com/livewire/livewire)
* [Sweetalert2](https://github.com/sweetalert2/sweetalert2)
* [Select2](https://github.com/select2/select2)
* [Bootstrap Datepicker](https://github.com/uxsolutions/bootstrap-datepicker)

## Instalation

1. Clone this repository.

 ```
 git clone https://gitlab.com/anggayudap/jcc-partnership-project-challenge.git
 ```
2. Get into 'aplikasi-kasir' directory

```
cd aplikasi-kasir
```
3. Install package from composer.

```
 composer install
 ```

 if fail, then use..

 ```
 composer update
 ```

4. Copy file .env.example to .env, then syncro with your database config.

file .env
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=kasir_jcc_anggayudap
DB_USERNAME=root
DB_PASSWORD=
```

5. Do migration and seed.

 ```
 php artisan migrate:fresh --seed
 ```

6. Run on local development server with.

 ```
 php artisan serve
 ```
7. Login with this credential.

| username | email | password | role |
| -------- | ----- | ---------| ---- |
| admin | admin@bogakasir.id | admin | Admin Kasir |
| kasir | kasir@bogakasir.id | kasir | Kasir |
