<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PembelianController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'authenticate'])->name('authenticate');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Route::prefix('')->middleware('auth')->group(function () {
    Route::get('/dashboard', [Controller::class, 'dashboard'])->name('dashboard');
    Route::put('/user/{id}/respass', [UserController::class, 'respass'])->name('user.respass');
    Route::get('/profil', [UserController::class, 'profile'])->name('user.profile');
    Route::put('/profil', [UserController::class, 'profile_update'])->name('user.profile_update');
    Route::resource('/user', UserController::class);
    Route::resource('/barang', BarangController::class);
    Route::prefix('/pembelian')->group(function () {
        Route::redirect('/', '/pembelian/mulai');
        Route::get('/mulai', [PembelianController::class, 'start'])->name('pembelian.start');
        Route::post('/', [PembelianController::class, 'bought'])->name('pembelian.bought');
        Route::get('/riwayat', [PembelianController::class, 'history'])->name('pembelian.history');
        Route::get('/{id}/detail', [PembelianController::class, 'detail'])->name('pembelian.detail');
        Route::get('/{id}/print', [PembelianController::class, 'print'])->name('pembelian.print');
    });
});